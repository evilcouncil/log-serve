package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"

	// external
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

var (
	port            int
	serverCertPath  string
	srvKeyPath      string
	caCertPath      string
	serverName      string
	logDataLocation string
	logFilePath     string
	logPattern      string
)

type FileList struct {
	CurrentLogFiles []string `json: "currentlogfiles"`
}

func loadConfigs() error {
	// Set config params
	viper.SetConfigType("env")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		log.Println("Error reading .env file for settings.")
		_, newErr := os.Stat(".env")
		if newErr == nil {
			return err
		}
		log.Println(".env file does not exist, continuing.")
	}
	serverCertPath = viper.GetString(SRVCERT)
	if serverCertPath == "" {
		return errors.New("SERVER_CERT not set")
	}

	srvKeyPath = viper.GetString(SRVKEY)
	if srvKeyPath == "" {
		return errors.New("SERVER_KEY not set")
	}

	caCertPath = viper.GetString(CACERT)
	if caCertPath == "" {
		return errors.New("CA_CERT not set")
	}

	serverName = viper.GetString(SRVNAME)
	if serverName == "" {
		return errors.New("SERVER_NAME not set")
	}

	logPattern = viper.GetString(LOG_PATTERN)
	if logPattern == "" {
		return errors.New("LOG_PATTERN not set")
	}

	viper.SetDefault(PORT, DEFAULT_PORT)
	port = viper.GetInt(PORT)

	viper.SetDefault(LOG_DATA_LOCATION, DEFAULT_DATA_LOCATION)
	logDataLocation = viper.GetString(LOG_DATA_LOCATION)

	viper.SetDefault(LOG_PATH, DEFAULT_LOG_PATH)
	logFilePath = viper.GetString(LOG_PATH)

	return nil
}

func getTLSConfig() *tls.Config {
	config := tls.Config{}

	caCert, err := ioutil.ReadFile(caCertPath)
	if err != nil {
		log.Fatal(err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	config.ServerName = serverName
	config.ClientAuth = tls.RequireAndVerifyClientCert
	config.ClientCAs = caCertPool
	config.MinVersion = tls.VersionTLS12

	return &config
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello this is web server"))
}

func getFileList() (*FileList, error) {
	newList := FileList{}

	files, err := ioutil.ReadDir(logDataLocation)
	if err != nil {
		return &newList, err
	}

	for _, currentFile := range files {
		matched, err := regexp.MatchString(logPattern, currentFile.Name())
		if err != nil {
			return &newList, err
		}
		if matched {
			newList.CurrentLogFiles = append(newList.CurrentLogFiles, currentFile.Name())
		}
	}

	return &newList, nil

}

func verifyFilePath(targetFile string) (bool, error) {
	fileList, err := getFileList()
	if err != nil {
		return false, err
	}

	for _, filename := range fileList.CurrentLogFiles {
		if targetFile == filename {
			return true, nil
		}
	}

	return false, nil

}

func handleListLogs(w http.ResponseWriter, r *http.Request) {
	currentFiles, err := getFileList()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	fileJson, err := json.Marshal(currentFiles)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write(fileJson)
}

func getLogFile(w http.ResponseWriter, r *http.Request) {
	targetFile := mux.Vars(r)["filename"]
	if fileExists, err := verifyFilePath(targetFile); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	} else {
		if !fileExists {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(fmt.Sprintf("File %s Not Found", targetFile)))
		}
	}

	data, err := os.ReadFile(logDataLocation + "/" + targetFile)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Write(data)
}

func deleteLogFile(w http.ResponseWriter, r *http.Request) {
	targetFile := mux.Vars(r)["filename"]
	if fileExists, err := verifyFilePath(targetFile); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
	} else {
		if !fileExists {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(fmt.Sprintf("File %s Not Found", targetFile)))
		}
	}

	if err := os.Remove(logDataLocation + "/" + targetFile); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("Success"))
}

func main() {
	log.Println("log-serve loading config")
	if err := loadConfigs(); err != nil {
		log.Fatal(err)
	}

	tlsConfig := getTLSConfig()

	//r := http.NewServeMux()
	r := mux.NewRouter()
	r.HandleFunc("/", handleIndex)
	r.HandleFunc("/v1/files", handleListLogs)
	r.HandleFunc("/v1/files/{filename}", getLogFile).Methods("GET")
	r.HandleFunc("/v1/files/{filename}", deleteLogFile).Methods("DELETE")

	server := &http.Server{
		Addr:         ":" + strconv.Itoa(port),
		ReadTimeout:  5 * time.Minute,
		WriteTimeout: 10 * time.Second,
		TLSConfig:    tlsConfig,
		Handler:      r,
	}

	if err := server.ListenAndServeTLS(serverCertPath, srvKeyPath); err != nil {
		log.Fatal(err)
	}

}
