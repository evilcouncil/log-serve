log-serve
---

Web server written for the sole purpose of serving log files. Clients must present valid certificates to 
access files.

### Environment Variables

```
SERVER_CERT - certificate for this service.
SERVER_KEY - matching key for server certificate.
CA_CERT - ca certificate to validate client certificates with.
SERVER_NAME - name of this server.
LOG_PATTERN - regex to match for serving logs
LOG_LOCATION - where logs to serve are located, default "."
LOG_PATH - where to write logs for this service, default "./log-serve.log".
PORT - what port number to listen on, default "8080"
```


[Sample docker-compose file](docker-compose.yml)

