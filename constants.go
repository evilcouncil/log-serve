package main

const (
	SRVCERT               = "SERVER_CERT"
	SRVKEY                = "SERVER_KEY"
	SRVNAME               = "SERVER_NAME"
	CACERT                = "CA_CERT"
	PORT                  = "PORT"
	LOG_DATA_LOCATION     = "LOG_DATA_LOCATION"
	LOG_PATTERN           = "LOG_PATTERN"
	LOG_PATH              = "LOG_PATH"
	DEFAULT_PORT          = 8080
	DEFAULT_DATA_LOCATION = "."
	DEFAULT_LOG_PATH      = "log-serve.log"
)
